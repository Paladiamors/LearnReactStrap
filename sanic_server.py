##################################################
# Author: Justin Ho
# Created: 2020-04-26
# Copyright (C) Justin Ho - All Rights Reserved
##################################################

from sanic import Sanic
from sanic.response import json


app = Sanic(name="main")

@app.route("/api/users")
async def users(request):
    return json({"users": ["user1", "user2", "user3", "user4"]})

@app.route("/api/users2")
async def users2(request):
    return json({"users": ["user1", "user2", "user3", "user4", "user5"]})

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8081, auto_reload=True)
import React from "react";
import ReactDOM from "react-dom";
import { Button } from "reactstrap";

export default class Users extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
    };
  }

  componentDidMount() {
    fetch("/api/users")
      .then((response) => response.json())
      .then((data) => this.setState({ data }));
  }

  refreshUsers = () => {
    fetch("/api/users2")
      .then((response) => response.json())
      .then((data) => this.setState({ data }));
  }

  render() {
    console.log(this.state.data);
    const users = ["user1", "user2", "user3x"];
    let userData;
    const printUser = (user) => <p key={user}>{user}</p>;
    userData = this.state.data
      ? this.state.data.users.map(printUser)
      : users.map(printUser);
    return (
      <div>
        <h3>Users</h3>
        <p>These are the users</p>
        {userData}
        <Button color="info" onClick={this.refreshUsers}>
          Refresh Users
        </Button>
      </div>
    );
  }
}

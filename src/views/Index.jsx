import React from "react";
import Users from "../components/Users";
import { Button } from "reactstrap";

const Index = () => (
  <div className="container">
    <h2>Welcome to the app</h2>
    <Button color="primary">Submit</Button>
    <Button color="secondary">Submit</Button>
    <Button color="success">Submit</Button>
    <Users />
  </div>
);

export default Index
import React from "react";
import ReactDOM from "react-dom";
import AppRouter from "./routers/AppRouter"
import "bootstrap/dist/css/bootstrap.css";
import "./assets/scss/_styles.scss";


ReactDOM.render(<AppRouter />, document.getElementById("root"));

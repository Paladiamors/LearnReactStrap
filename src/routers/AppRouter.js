import React from "react";
import { BrowserRouter, Route, Switch, Link, NavLink } from "react-router-dom";
import Index from "../views/Index";
import Contact from "../views/Contact";
import About from "../views/About";
import NotFoundPage from "../views/NotFoundPage";

const Header = () => (
  <header>
    <NavLink to="/" activeClassName="is-active" exact={true}>
      Index
    </NavLink>
    <NavLink to="/About" activeClassName="is-active" exact={true}>
      About
    </NavLink>
    <NavLink to="/Contact" activeClassName="is-active" exact={true}>
      Contact
    </NavLink>
  </header>
);

const AppRouter = () => (
  <BrowserRouter>
    <Header />
    <Switch>
      <Route path="/" component={Index} exact={true} />
      <Route path="/About" component={About} exact={true} />
      <Route path="/Contact" component={Contact} exact={true} />
      <Route component={NotFoundPage} />
    </Switch>
  </BrowserRouter>
);

export default AppRouter;
